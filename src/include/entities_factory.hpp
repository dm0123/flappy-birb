#pragma once

namespace flappy_clone
{
class Game;

class EntitiesFactory
{
public:
    EntitiesFactory(Game& g) : m_game(g)
    {
    }

    void MakeBirb() const;
    void MakePipes() const;
    void MakeBase() const;
    void MakeBackground() const;
private:
    Game& m_game;
};
} // namespace flappy_clone
