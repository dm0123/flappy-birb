#pragma once

#include <core/app.hpp>
#include <core/events.hpp>
#include <core/input.hpp>
#include <core/screen.hpp>
#include <core/scores.hpp>
#include <core/sound.hpp>
#include <core/settings.hpp>
#include <entities_factory.hpp>
#include <screen_factory.hpp>
#include <SFML/System/Clock.hpp>
#include <entity.hpp>

#include <memory>
#include <vector>

namespace flappy_clone
{
class Game
{
public:
    friend class EntitiesFactory;
    friend class ScreenFactory;

    enum class State : int
    {
        MainMenu, Playing, Paused
    };

    struct GameData
    {
        EntityId bird { INVALID_ENTITY };
        EntityId pipes { INVALID_ENTITY };
        EntityId background { INVALID_ENTITY };
        EntityId base { INVALID_ENTITY };
    };

    Game();
    ~Game() = default;

    void Init();
    void Run();
    void Shutdown();

    void AddStateChangeListener(core::EventHandler<State> state_changed);
    void AddTickEventListener(core::EventHandler<> tick_listener);

    void OnStateChange(State new_state);

private:
    void LoadAssets();
    void MakeEntities();
    void InitScreens();

    bool m_finished = false; // TODO: may be atomic_bool in multithreaded environment

    State m_state;

    core::App m_app;
    core::Input m_input;
    std::vector<core::Screen> m_screens;
    size_t m_current_screen_index;
    core::Scores m_scores;
    int m_current_score;
    std::string m_current_player_name;

    core::Sound m_sound;
    core::Settings m_settings;

    ScreenFactory m_screen_factory;
    EntitiesFactory m_entities_factory;

    core::EventHandler<> m_finished_handler;
    core::EventHandler<State> m_state_changed_handler; // This is handler for game logic state change

    core::Event<State> m_state_event; // This is event firing when state is changed from handler above
    core::Event<> m_tick_event;

    sf::Clock m_clock;

    GameData m_data;
};
} // namespace flappy_clone
