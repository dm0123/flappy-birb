#pragma once
#include <core/screen.hpp>

namespace flappy_clone
{
class Game;
/// Helper for making various game screens
class ScreenFactory
{
public:
    ScreenFactory(Game const& g) : m_game(g)
    {
    }

    ~ScreenFactory() = default;

    core::Screen MakeMainMenuScreen();
    core::Screen MakeGameScreen();
    core::Screen MakePauseScreen();
    core::Screen MakeSettingsScreen();
    core::Screen MakeScoresScreen();
private:
    Game const& m_game;
};
}
