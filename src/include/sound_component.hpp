#pragma once
#include <component.hpp>

namespace flappy_clone
{
class SoundComponent : public AbstractComponent
{
public:
    void Init() override;
    void AddAsset(std::string_view asset_name) override;
};
} // namespace flappy_clone
