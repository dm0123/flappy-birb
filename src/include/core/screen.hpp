#pragma once
#include <vector>
#include <string>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <component.hpp>

namespace flappy_clone::core
{
class IDrawable
{
public:
    IDrawable() = default;

    void SetPosition(sf::Vector2i pos) { m_screen_pos = pos; }

    virtual ~IDrawable() = default;
    virtual void Draw(sf::RenderWindow& window) {}
protected:
    sf::Vector2i m_screen_pos;
};


class SimpleSpriteItem : public IDrawable
{
public:
    SimpleSpriteItem() = default;
    void Draw(sf::RenderWindow& window) override;
    void SetSprite(sf::Sprite const& sprite) noexcept { m_sprite = sprite; }
private:
    sf::Sprite m_sprite;
};

class Screen
{
public:
    Screen() = default;
    ~Screen() = default;

    void AddEntity(EntityId e);
    void Draw(sf::RenderWindow& window);
protected:
    std::vector<IDrawable*> m_drawables;
};
} // namespace flappy_clone::core
