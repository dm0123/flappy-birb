#include <screen_factory.hpp>
#include <game.hpp>

namespace flappy_clone
{
core::Screen ScreenFactory::MakeMainMenuScreen()
{
    core::Screen screen;
    return screen;
}

core::Screen ScreenFactory::MakeGameScreen()
{
    core::Screen screen;
    screen.AddEntity(m_game.m_data.bird);
    screen.AddEntity(m_game.m_data.background);
    screen.AddEntity(m_game.m_data.base);
    screen.AddEntity(m_game.m_data.pipes);

    return screen;
}

core::Screen ScreenFactory::MakePauseScreen()
{
    core::Screen screen;
    return screen;
}

core::Screen ScreenFactory::MakeSettingsScreen()
{
    core::Screen screen;
    return screen;
}

core::Screen ScreenFactory::MakeScoresScreen()
{
    core::Screen screen;
    return screen;
}
} // namespace flappy_clone
