#include <sprite_component.hpp>

namespace flappy_clone
{
TextureAsset::TextureAsset(std::string name, std::string path)
    : core::AbstractAsset(std::move(name), std::move(path))
{
}

void TextureAsset::Load()
{
    m_texture.loadFromFile(m_path);
}

void SpriteComponent::Init()
{
}

void SpriteComponent::AddAsset(std::string_view name)
{
    m_asset = core::AssetManager::Instance().GetHandleByName(name);
    TextureAsset const& texture = static_cast<TextureAsset const&>(core::AssetManager::Instance().GetByName(name));
    m_drawable.SetSprite(sf::Sprite(texture.Texture()));
}

core::IDrawable* SpriteComponent::MakeDrawable()
{
    return &m_drawable;
}
} // namespace flappy_clone
