#include <entities_factory.hpp>
#include <entity.hpp>
#include <sprite_component.hpp>
#include <game.hpp>

#include <vector>
#include <memory>
#include <string_view>

using namespace std::literals;

namespace flappy_clone
{
void EntitiesFactory::MakeBirb() const
{
    auto& ecs_instance = ECSManager::Instance();
    std::vector<std::unique_ptr<AbstractComponent>> bird_components;
    bird_components.reserve(3);

    std::unique_ptr<SpriteComponent> bird_downflap = std::make_unique<SpriteComponent>();
    bird_downflap->AddAsset("bird_downflap"sv);
    bird_components.push_back(std::move(bird_downflap));
    std::unique_ptr<SpriteComponent> bird_upflap = std::make_unique<SpriteComponent>();
    bird_upflap->AddAsset("bird_upflap"sv);
    bird_components.push_back(std::move(bird_upflap));
    std::unique_ptr<SpriteComponent> bird_midflap = std::make_unique<SpriteComponent>();
    bird_midflap->AddAsset("bird_midflap");
    bird_components.push_back(std::move(bird_midflap));

    EntityId birb = ecs_instance.MakeEntity("bird", std::move(bird_components));
    m_game.m_data.bird = birb;
}

void EntitiesFactory::MakePipes() const
{
    auto& ecs_instance = ECSManager::Instance();
    std::vector<std::unique_ptr<AbstractComponent>> components;
    components.reserve(1);

    std::unique_ptr<SpriteComponent> pipe_green = std::make_unique<SpriteComponent>();
    pipe_green->AddAsset("pipe_green"sv);

    auto pipe = ecs_instance.MakeEntity("pipe", std::move(components));
    m_game.m_data.pipes = pipe;
}

void EntitiesFactory::MakeBase() const
{
    auto& ecs_instance = ECSManager::Instance();
    std::vector<std::unique_ptr<AbstractComponent>> components;
    components.reserve(1);

    std::unique_ptr<SpriteComponent> base = std::make_unique<SpriteComponent>();
    base->AddAsset("base"sv);

    auto base_e = ecs_instance.MakeEntity("base", std::move(components));
    m_game.m_data.base = base_e;
}

void EntitiesFactory::MakeBackground() const
{
    auto& ecs_instance = ECSManager::Instance();
    std::vector<std::unique_ptr<AbstractComponent>> components;
    components.reserve(1);

    std::unique_ptr<SpriteComponent> background = std::make_unique<SpriteComponent>();
    background->AddAsset("background_day"sv);

    auto background_e = ecs_instance.MakeEntity("background", std::move(components));
    m_game.m_data.background = background_e;
}
} // namespace flappy_clone
