#include <core/screen.hpp>
#include <entity.hpp>

namespace flappy_clone::core
{
void SimpleSpriteItem::Draw(sf::RenderWindow& window)
{
    window.draw(m_sprite);
}

void Screen::AddEntity(EntityId e)
{
   std::vector<core::IDrawable*> drawables = ECSManager::Instance().CollectDrawables(e);
   m_drawables.reserve(m_drawables.size() + std::distance(drawables.begin(), drawables.end()));
   m_drawables.insert(m_drawables.end(), drawables.begin(), drawables.end());
}

void Screen::Draw(sf::RenderWindow& window)
{
    for(IDrawable* d : m_drawables)
        if(d)
            d->Draw(window);
}

} // namespace flappy_clone::core
