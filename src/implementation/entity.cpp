#include <entity.hpp>
#include <core/app_exception.hpp>

namespace flappy_clone
{
ECSManager& ECSManager::Instance()
{
    static ECSManager instance;
    return instance;
}

EntityId ECSManager::MakeEntity(std::string name, std::vector<std::unique_ptr<AbstractComponent>>&& components)
{
    Entity new_e(std::move(name));
    EntityId new_id = m_entities.size();
    for(std::unique_ptr<AbstractComponent>& component : components)
    {
        component->SetParent(new_id);
        new_e.m_components.push_back(std::move(component));
    }
    m_entities_by_name[std::string{ new_e.Name() }] = new_id;
    m_entities.push_back(std::move(new_e));
    return new_id;
}

Entity& ECSManager::FindByName(std::string_view name)
{
    auto it = m_entities_by_name.find(std::string{ name });
    if(it == m_entities_by_name.end())
        throw core::AppException("ECSManager: Failed to find entity by id");
    return m_entities[it->second];
}

void ECSManager::AddComponent(EntityId id, std::unique_ptr<AbstractComponent>&& c)
{
    m_entities[id].AddComponent(std::move(c));
}

std::vector<core::IDrawable*> ECSManager::CollectDrawables(EntityId e_id)
{
    std::vector<core::IDrawable*> result;
    auto& e = m_entities[e_id];
    for(std::unique_ptr<AbstractComponent> const& component_ptr : e.m_components)
    {
        if(component_ptr && component_ptr->MakeDrawable())
            result.push_back(component_ptr->MakeDrawable());
    }
    return result;
}

ECSManager::ECSManager() : m_game_tick_handler([this]()
                           {
                               for(Entity& e : m_entities)
                                   for(std::unique_ptr<AbstractComponent>& component_ptr : e.m_components)
                                       if(component_ptr)
                                           component_ptr->Update();
                           })
{
}

Entity::Entity() = default;

Entity::Entity(std::string name) : m_name(std::move(name))
{
}

void Entity::AddComponent(std::unique_ptr<AbstractComponent>&& c)
{
    m_components.push_back(std::move(c));
}

Entity::~Entity()
{
}
} // namespace flappy_clone
