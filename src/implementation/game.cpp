#include <game.hpp>
#include <core/asset_manager.hpp>
#include <sprite_component.hpp>

using namespace std::literals;

namespace flappy_clone
{
namespace
{
    // lock at 60 frames per second
    const auto TIME_PER_FRAME = sf::seconds( 1.0f / 60.0f );

    enum  Screens : size_t
    {
        sMainMenu,
        sGame,
        sPause,
        sSettings,
        sScores
    };
}

Game::Game() : m_screen_factory(*this),
               m_entities_factory(*this), m_finished_handler([this]()
            {
                m_finished = true;
            }),
               m_state_changed_handler([this](State new_state)
            {
                m_state = new_state;
            })
{
}

void Game::Init()
{
    m_app.Init();
    m_app.AddFinishedEventHandler(m_finished_handler);

    LoadAssets();
    MakeEntities();
    InitScreens();
}

void Game::Run()
{
    sf::Time time_since_last_update = sf::Time::Zero;
    for(;;)
    {
        m_app.Run();
        time_since_last_update += m_clock.restart();
        while(time_since_last_update > TIME_PER_FRAME)
        {
            time_since_last_update -= TIME_PER_FRAME;
            if(m_state == State::Playing)
                m_tick_event();
        }
        if(m_screens.size() > 0 && m_current_screen_index < m_screens.size())
            m_screens[m_current_screen_index].Draw(m_app.Window());
        if(m_finished)
            break;
    }
}

void Game::Shutdown()
{
    m_app.Shutdown();
}

void Game::AddStateChangeListener(core::EventHandler<State> state_changed)
{
    m_state_event += state_changed;
}

void Game::OnStateChange(State new_state)
{
    m_state = new_state;
    switch(m_state)
    {
    case State::Paused:
        m_current_screen_index = Screens::sPause;
        break;
    case State::Playing:
        m_current_screen_index = Screens::sGame;
        break;
    case State::MainMenu:
        m_current_screen_index = Screens::sMainMenu;
        break;
    }
}

void Game::LoadAssets()
{
    // TODO: this should be loaded from file, but we hardcode assets for now
    auto& assets = core::AssetManager::Instance();

    // birb assets
    assets.MakeAsset<TextureAsset>("bird_downflap"sv, "assets/sprites/redbird-downflap.png"sv);
    assets.MakeAsset<TextureAsset>("bird_midflap"sv, "assets/sprites/redbird-midflap.png"sv);
    assets.MakeAsset<TextureAsset>("bird_upflap"sv, "assets/sprites/redbird-upflap.png"sv);

    // background
    assets.MakeAsset<TextureAsset>("background_day"sv, "assets/sprites/background-day.png"sv);

    // base
    assets.MakeAsset<TextureAsset>("base"sv, "assets/sprites/base.png"sv);

    // pipe
    assets.MakeAsset<TextureAsset>("pipe_green"sv, "assets/sprites/pipe-green.png"sv);

    // logo
    assets.MakeAsset<TextureAsset>("logo"sv, "assets/sprites/message.png");

    assets.Load();
}

void Game::MakeEntities()
{
    m_entities_factory.MakeBirb();
    m_entities_factory.MakeBase();
    m_entities_factory.MakeBackground();
    m_entities_factory.MakePipes();
}

void Game::InitScreens()
{
//    m_screens.push_back(m_screen_factory.MakeMainMenuScreen());
    m_screens.push_back(m_screen_factory.MakeGameScreen());
//    m_screens.push_back(m_screen_factory.MakePauseScreen());
//    m_screens.push_back(m_screen_factory.MakeScoresScreen());
}
} // namespace flappy_clone
