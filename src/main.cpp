#include <game.hpp>

int main(int /*argc*/, char** /*argv*/)
{
    flappy_clone::Game g;
    g.Init();
    g.Run();
    g.Shutdown();
    return 0;
}
